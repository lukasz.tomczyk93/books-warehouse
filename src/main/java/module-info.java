module com.warehouse.bookswarehouse {
    requires javafx.controls;
    requires javafx.fxml;
    requires log4j;
    requires java.sql;

    opens com.warehouse.books to javafx.fxml;
    opens com.warehouse.books.controller to javafx.fxml;
    opens com.warehouse.books.model to javafx.base;

    exports com.warehouse.books;
    exports com.warehouse.books.controller;
}