package com.warehouse.books.exceptions;

public class InvalidBookDataException extends RuntimeException {

    public InvalidBookDataException(String message) {
        super(message);
    }
}
