package com.warehouse.books.service;

import com.warehouse.books.exceptions.BookNotFoundException;
import com.warehouse.books.exceptions.InvalidBookDataException;
import com.warehouse.books.model.Book;
import com.warehouse.books.repository.BooksRepository;
import com.warehouse.books.repository.BooksRepositoryFactory;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class BooksService {

    private static final Logger LOGGER = Logger.getLogger(BooksService.class);
    private static final BooksService INSTANCE = new BooksService();

    private final BooksRepository booksRepository = BooksRepositoryFactory.create(BooksRepository.Type.JDBC_MYSQL);

    private BooksService() {
        //Only for singleton purpose
    }

    public static BooksService getInstance() {
        return INSTANCE;
    }

    public List<Book> getAll() {
        return booksRepository.getAll();
    }

    public void deleteById(UUID id) {
        Optional<Book> book = booksRepository.findOneById(id);
        if (book.isEmpty()) {
            LOGGER.warn("Tried remove not existing book");
            return;
        }
        booksRepository.deleteById(id);
    }

    public Book findOneById(UUID id) throws BookNotFoundException {
        Optional<Book> book = booksRepository.findOneById(id);
        return book.orElseThrow(() -> {
            String bookNotFoundMessage = String.format("Could  not find boo by id: %s", id);
            LOGGER.warn(bookNotFoundMessage);
            throw new BookNotFoundException(bookNotFoundMessage);
        });
    }

    public void create(Book book) {
        validateDataOrThrow(book);
        booksRepository.create(book);
    }

    public void update(Book book) {
        validateDataOrThrow(book);
        booksRepository.update(book);
    }

    private void validateDataOrThrow(Book book) {
        String author = book.getAuthor();
        String title = book.getTitle();

        checkOrThrow(author, "Author cannot be empty");
        checkOrThrow(title, "Title cannot be empty");
    }

    private void checkOrThrow(String property, String errorMessage) {
        if (property == null || property.isBlank()) {
            throw new InvalidBookDataException(errorMessage);
        }
    }
}
