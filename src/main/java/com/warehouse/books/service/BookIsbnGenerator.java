package com.warehouse.books.service;

import java.util.UUID;

public class BookIsbnGenerator {

    private BookIsbnGenerator() {
        throw new IllegalStateException("Cannot instantiate this class - use its static methods");
    }

    public static UUID generate() {
        return UUID.randomUUID();
    }
}
