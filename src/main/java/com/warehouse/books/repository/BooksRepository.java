package com.warehouse.books.repository;

import com.warehouse.books.model.Book;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BooksRepository {

    enum Type {
        IN_MEMORY,
        JDBC_MYSQL
    }

    List<Book> getAll();

    void deleteById(UUID id);

    Optional<Book> findOneById(UUID id);

    void create(Book book);

    void update(Book book);
}
