package com.warehouse.books.repository;

import com.warehouse.books.model.Book;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

class MySqlJdbcBooksRepository implements BooksRepository {

    private static final Logger LOGGER = Logger.getLogger(MySqlJdbcBooksRepository.class);

    private final Connection connection;

    MySqlJdbcBooksRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Book> getAll() {
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from books");
            List<Book> allBooks = new ArrayList<>();
            while (resultSet.next()) {
                Book book = mapFromResultSet(resultSet);
                allBooks.add(book);
            }
            return allBooks;
        } catch (SQLException exception) {
            LOGGER.error("Error occurred", exception);
            return Collections.emptyList();
        }
    }

    private Book mapFromResultSet(ResultSet resultSet) throws SQLException {
        UUID isbn = UUID.fromString(resultSet.getString("isbn"));
        String title = resultSet.getString("title");
        String author = resultSet.getString("author");
        String description = resultSet.getString("description");
        return new Book(isbn, author, title, description);
    }

    @Override
    public void deleteById(UUID id) {
        String deleteSql = "delete from books b where b.isbn = ?";
        try (var preparedStatement = connection.prepareStatement(deleteSql)) {
            LOGGER.info("Deleting book from MySQL DB");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            LOGGER.error("Error occurred", exception);
        }
    }

    @Override
    public Optional<Book> findOneById(UUID id) {
        String selectBookSql = "select * from books b where b.isbn = ?";
        try (var preparedStatement = connection.prepareStatement(selectBookSql)) {
            LOGGER.info("Retrieving book from MySQL DB");
            preparedStatement.setString(1, String.valueOf(id));
            var resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Book book = mapFromResultSet(resultSet);
                return Optional.of(book);
            } else {
                return Optional.empty();
            }
        } catch (SQLException exception) {
            LOGGER.error("Error occurred", exception);
            return Optional.empty();
        }
    }

    @Override
    public void create(Book book) {
        String insertBook = """
                insert into books (isbn, title, author, `description`)
                values (?, ?, ?, ?)
                """;
        try (var preparedStatement = connection.prepareStatement(insertBook)) {
            preparedStatement.setString(1, String.valueOf(book.getIsbn()));
            preparedStatement.setString(2, book.getTitle());
            preparedStatement.setString(3, book.getAuthor());
            preparedStatement.setString(4, book.getDescription());
            preparedStatement.executeUpdate();
            LOGGER.info("Inserting book");
        } catch (SQLException exception) {
            LOGGER.error("Error occurred", exception);
        }
    }

    @Override
    public void update(Book book) {
        String updateBook = """
                update books b
                set b.title = ?,
                b.author = ?,
                b.`description` = ?
                where b.isbn = ?
                """;
        try (var preparedStatement = connection.prepareStatement(updateBook)) {
            LOGGER.info("Updating book");
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setString(3, book.getDescription());
            preparedStatement.setString(4, String.valueOf(book.getIsbn()));
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            LOGGER.error("Error occurred", exception);
        }
    }
}
