package com.warehouse.books.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class JdbcConnectionFactory {

    static Connection createMySqlConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/java_training", "warehouse_admin", "Start123");
    }
}
