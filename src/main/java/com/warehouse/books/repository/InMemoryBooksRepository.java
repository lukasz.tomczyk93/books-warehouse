package com.warehouse.books.repository;

import com.warehouse.books.model.Book;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.*;

class InMemoryBooksRepository implements BooksRepository {

    private static final Logger LOGGER = Logger.getLogger(InMemoryBooksRepository.class);

    private static final Map<UUID, Book> BOOKS = new HashMap<>();

    @Override
    public List<Book> getAll() {
        //return new ArrayList<>(BOOKS.values());
        //return BOOKS.values().stream().toList();
        LOGGER.info("Returning all books");
        return List.copyOf(BOOKS.values());
    }

    @Override
    public void deleteById(UUID id) {
        LOGGER.info(MessageFormat.format("Deleting book by id={0}", id));
        BOOKS.remove(id);
    }

    @Override
    public Optional<Book> findOneById(UUID id) {
        LOGGER.info(MessageFormat.format("Retrieving book by id={0}", id));
        var book = BOOKS.get(id);
        return Optional.ofNullable(book);
    }

    @Override
    public void create(Book book) {
        LOGGER.info("Creating book");
        BOOKS.putIfAbsent(book.getIsbn(), book);
    }

    @Override
    public void update(Book book) {
        LOGGER.info("Updating book");
        BOOKS.put(book.getIsbn(), book);
    }
}
