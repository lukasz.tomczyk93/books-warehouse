package com.warehouse.books.repository;

import com.warehouse.books.exceptions.CouldNotCreateRepositoryException;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.text.MessageFormat;

public class BooksRepositoryFactory {

    private static final Logger LOGGER = Logger.getLogger(BooksRepositoryFactory.class);

    private BooksRepositoryFactory() {
        throw new IllegalStateException("Cannot instantiate this class - use its static methods");
    }

    public static BooksRepository create(BooksRepository.Type type) {
        return switch (type) {
            case IN_MEMORY -> new InMemoryBooksRepository();
            case JDBC_MYSQL -> {
                try {
                    yield new MySqlJdbcBooksRepository(JdbcConnectionFactory.createMySqlConnection());
                } catch (SQLException exception) {
                    LOGGER.error(MessageFormat.format("Could not create MySql connection. The root cause: {0}", exception.getMessage()));
                    throw new CouldNotCreateRepositoryException();
                }
            }
            default -> throw new IllegalStateException(String.format("Unsupported repository type: %s", type));
        };
    }
}
