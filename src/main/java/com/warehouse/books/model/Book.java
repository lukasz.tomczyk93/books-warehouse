package com.warehouse.books.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.UUID;

public class Book {

    private final UUID isbn;
    private final StringProperty author;
    private final StringProperty title;
    private final StringProperty description;

    public Book(UUID isbn, String author, String title, String description) {
        this.isbn = isbn;
        this.author = new SimpleStringProperty(author);
        this.title = new SimpleStringProperty(title);
        this.description = new SimpleStringProperty(description);
    }

    public UUID getIsbn() {
        return isbn;
    }

    public String getAuthor() {
        return author.get();
    }

    public String getTitle() {
        return title.get();
    }

    public String getDescription() {
        return description.get();
    }
}
