package com.warehouse.books.controller;

import com.warehouse.books.model.Book;
import com.warehouse.books.service.BooksService;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.log4j.Logger;

import java.util.List;

public class AllBooksController {

    private static final Logger LOGGER = Logger.getLogger(AllBooksController.class);

    @FXML
    private TableView<Book> booksTable;

    @FXML
    private TableColumn<Book, String> columnTitle;

    @FXML
    private TableColumn<Book, String> columnAuthor;

    @FXML
    private TableColumn<Book, String> columnDescription;

    @FXML
    private TableColumn<Book, Void> columnAction;

    private static final int TITLE_WIDTH_PERCENTAGE = 25;
    private static final int AUTHOR_WIDTH_PERCENTAGE = 25;
    private static final int DESCRIPTION_WIDTH_PERCENTAGE = 25;
    private static final int ACTION_WIDTH_PERCENTAGE = 25;

    private BooksService booksService = BooksService.getInstance();

    @FXML
    private void initialize() {
        booksTable.setEditable(false);

        LOGGER.info("Resizing columns alignment");
        columnTitle.prefWidthProperty().bind(booksTable.widthProperty().multiply(0.01 * TITLE_WIDTH_PERCENTAGE));
        columnAuthor.prefWidthProperty().bind(booksTable.widthProperty().multiply(0.01 * AUTHOR_WIDTH_PERCENTAGE));
        columnDescription.prefWidthProperty().bind(booksTable.widthProperty().multiply(0.01 * DESCRIPTION_WIDTH_PERCENTAGE));
        columnAction.prefWidthProperty().bind(booksTable.widthProperty().multiply(0.01 * ACTION_WIDTH_PERCENTAGE));

        columnTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
        columnAuthor.setCellValueFactory(new PropertyValueFactory<>("author"));
        columnDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        columnAction.setCellFactory(tableColumn -> new ActionCell(booksService));

        List<Book> allBooks = booksService.getAll();
        booksTable.setItems(FXCollections.observableList(allBooks));
    }
}
