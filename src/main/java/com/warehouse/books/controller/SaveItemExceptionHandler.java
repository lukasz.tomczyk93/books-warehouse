package com.warehouse.books.controller;

import com.warehouse.books.exceptions.InvalidBookDataException;
import javafx.scene.control.Alert;

class SaveItemExceptionHandler {

    static void showAlert(InvalidBookDataException exception) {
        Alert validationAlert = new Alert(Alert.AlertType.ERROR);
        validationAlert.setContentText(exception.getMessage());
        validationAlert.show();
    }
}
