package com.warehouse.books.controller;

import com.warehouse.books.model.Book;
import com.warehouse.books.service.BooksService;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;
import org.apache.log4j.Logger;

import java.util.List;

public class MainViewController {

    private static final Logger LOGGER = Logger.getLogger(MainViewController.class);

    @FXML
    private Tab allBooksTab;

    @FXML
    void onTabEntered() {
        if (!allBooksTab.isSelected()) {
            return;
        }
        LOGGER.info("Refreshing books table");

        Node content = allBooksTab.getContent();
        Node foundNode = content.lookup("#booksTable");
        if (foundNode instanceof TableView) {
            TableView<Book> booksTable = (TableView<Book>) foundNode;
            List<Book> allBooks = BooksService.getInstance().getAll();
            booksTable.setItems(FXCollections.observableList(allBooks));
        }
    }
}
