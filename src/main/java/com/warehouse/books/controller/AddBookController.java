package com.warehouse.books.controller;

import com.warehouse.books.exceptions.InvalidBookDataException;
import com.warehouse.books.model.Book;
import com.warehouse.books.service.BookIsbnGenerator;
import com.warehouse.books.service.BooksService;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.log4j.Logger;

import java.util.UUID;

public class AddBookController {

    private static final Logger LOGGER = Logger.getLogger(AddBookController.class);

    private final BooksService booksService = BooksService.getInstance();

    @FXML
    private TextField titleInput;

    @FXML
    private TextField authorInput;

    @FXML
    private TextArea descriptionInput;

    @FXML
    void onSaveClicked() {
        LOGGER.info("Saving book");
        UUID isbn = BookIsbnGenerator.generate();
        String title = titleInput.getText();
        String author = authorInput.getText();
        String description = descriptionInput.getText();

        var book = new Book(isbn, author, title, description);
        try {
            booksService.create(book);
        } catch (InvalidBookDataException exception) {
            SaveItemExceptionHandler.showAlert(exception);
        }
    }
}
