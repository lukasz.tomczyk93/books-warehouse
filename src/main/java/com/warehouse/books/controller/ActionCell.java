package com.warehouse.books.controller;

import com.warehouse.books.WarehouseApplicationInitializer;
import com.warehouse.books.model.Book;
import com.warehouse.books.service.BooksService;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

class ActionCell extends TableCell<Book, Void> {

    private static final Logger LOGGER = Logger.getLogger(ActionCell.class);

    private final BooksService booksService;

    ActionCell(BooksService booksService) {
        this.booksService = booksService;
    }

    @Override
    protected void updateItem(Void item, boolean empty) {
        if (empty) {
            setGraphic(null);
        } else {
            var anchorPane = loadActionView();
            setGraphic(anchorPane);

            var editButton = (Button) anchorPane.lookup("#editButton");
            var deleteButton = (Button) anchorPane.lookup("#deleteButton");

            deleteButton.setOnAction(this::handleDelete);
            editButton.setOnAction(new EditBookEventHandler(loadEditView(), getTableView(), getIndex(), booksService));
        }
    }

    private void handleDelete(ActionEvent event) {
        LOGGER.info("Clicked delete button");
        TableView<Book> allBooksTable = getTableView();
        Book book = allBooksTable.getItems().get(getIndex());
        UUID isbn = book.getIsbn();
        booksService.deleteById(isbn);
        refreshTableAfterDelete(allBooksTable);
    }

    private AnchorPane loadActionView() {
        try {
            return FXMLLoader.load(Objects.requireNonNull(WarehouseApplicationInitializer.class.getResource("books-table-action-view.fxml")));
        } catch (IOException e) {
            LOGGER.error("Could not load action view");
            throw new RuntimeException(e);
        }
    }

    private AnchorPane loadEditView() {
        try {
            return FXMLLoader.load(Objects.requireNonNull(WarehouseApplicationInitializer.class.getResource("books-table-edit-view.fxml")));
        } catch (IOException e) {
            LOGGER.error("Could not load action view");
            throw new RuntimeException(e);
        }
    }

    private void refreshTableAfterDelete(TableView<Book> allBooksTable) {
        allBooksTable.getItems().remove(getIndex());
    }
}
