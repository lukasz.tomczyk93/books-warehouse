package com.warehouse.books.controller;

import com.warehouse.books.exceptions.InvalidBookDataException;
import com.warehouse.books.model.Book;
import com.warehouse.books.service.BooksService;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.UUID;

class EditBookEventHandler implements EventHandler {

    private final AnchorPane editItemView;
    private final TableView<Book> booksTable;
    private final int currentItemIndex;
    private final BooksService booksService;

    EditBookEventHandler(AnchorPane editItemView, TableView<Book> booksTable, int currentItemIndex, BooksService booksService) {
        this.editItemView = editItemView;
        this.booksTable = booksTable;
        this.currentItemIndex = currentItemIndex;
        this.booksService = booksService;
    }

    @Override
    public void handle(Event event) {
        TextField titleInput = (TextField) editItemView.lookup("#titleInput");
        TextField authorInput = (TextField) editItemView.lookup("#authorInput");
        TextArea descriptionInput = (TextArea) editItemView.lookup("#descriptionInput");
        Button updateButton = (Button) editItemView.lookup("#updateButton");

        ObservableList<Book> books = booksTable.getItems();
        UUID currentBookId = books.get(currentItemIndex).getIsbn();
        Book currentBook = booksService.findOneById(currentBookId);

        titleInput.setText(currentBook.getTitle());
        authorInput.setText(currentBook.getAuthor());
        descriptionInput.setText(currentBook.getDescription());

        Stage editDialog = new Stage();
        Scene dialogScene = new Scene(editItemView);
        editDialog.setScene(dialogScene);
        editDialog.show();

        updateButton.setOnAction(actionEvent -> {
            String newTitle = titleInput.getText();
            String newAuthor = authorInput.getText();
            String newDescription = descriptionInput.getText();
            Book updatedBook = new Book(currentBookId, newAuthor, newTitle, newDescription);
            try {
                booksService.update(updatedBook);
                editDialog.close();
                refreshTableAfterEdit(updatedBook);
            } catch (InvalidBookDataException exception) {
                SaveItemExceptionHandler.showAlert(exception);
            }
        });
    }

    private void refreshTableAfterEdit(Book updatedBook) {
        ObservableList<Book> allBooks = booksTable.getItems();
        allBooks.set(currentItemIndex, updatedBook);
    }
}
